package svt;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import svt.model.Role;
import svt.model.User;

public class Main {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        Role role = new Role();
        role.setName("User");
        Role role2 = new Role();
        role2.setName("Admin");
        User user = new User();
        user.setUsername("admin");
        user.setPassword("111");
        user.setFirstName("admin");
        user.setLastName("admin");
        user.setEmail("aa@aaa.com");
        user.setSex("Мужской");
        user.setRole(role2);
        try{
            session.beginTransaction();
            session.save(role);
            session.save(role2);
            session.save(user);
            session.getTransaction().commit();
        }catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }

        session.close();
        sessionFactory.close();
    }

}
